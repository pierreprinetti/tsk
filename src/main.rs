// task is a toy implementation of taskwarrior, written in Rust and using Automerge for keeping and
// sharing state.
//
// In its first milestone, it implements:
//   * task add 'Paint the door'
//     # Created task 1.
//   * task 1 done
//     # Completed task 1 'Paint the door'.
//     # Compeleted 1 task.
//
//     Setting a task as done sets its status to `completed`, adds an `end` date and updates the
//     `modified` date.

use anyhow::{anyhow, Result};
use automerge_repo::{DocumentId, StorageError};
use autosurgeon::{hydrate, reconcile};
use clap::{arg, command, ArgAction, Command};
use futures::future::BoxFuture;
use std::collections::HashMap;
use std::env;
use std::path::PathBuf;
use task::{Task, Tasks};

mod task {
    use autosurgeon::{Hydrate, Reconcile};
    use chrono::{DateTime, Local};
    use std::fmt;

    #[derive(Eq, Hash, PartialEq)]
    pub enum Status {
        New,
        InProgress,
        Done,
    }

    fn time_serialise(t: DateTime<Local>) -> String {
        t.to_rfc3339()
    }

    fn now() -> DateTime<Local> {
        Local::now()
    }

    fn now_serialised() -> String {
        time_serialise(now())
    }

    fn time_deserialise(s: &str) -> DateTime<Local> {
        DateTime::parse_from_rfc3339(s).unwrap().into()
    }

    impl fmt::Display for Status {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let repr = match self {
                Status::New => "New",
                Status::InProgress => "In progress",
                Status::Done => "Done",
            };
            write!(f, "{}", repr)
        }
    }

    impl Status {
        pub fn symbol(&self) -> &str {
            match self {
                Status::New => "NEW",
                Status::InProgress => "WIP",
                Status::Done => "DONE",
            }
        }
    }

    #[derive(Default, Debug, Clone, Reconcile, Hydrate, PartialEq)]
    pub struct Annotation {
        created: String,
        text: String,
    }

    impl fmt::Display for Annotation {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{}", self.text)
        }
    }

    impl Annotation {
        pub fn created(&self) -> DateTime<Local> {
            time_deserialise(&self.created)
        }
    }

    #[derive(Default, Debug, Clone, Reconcile, Hydrate, PartialEq)]
    pub struct Task {
        pub description: String,
        pub start: Option<String>,
        pub end: Option<String>,
        pub annotations: Vec<Annotation>,
    }

    impl Task {
        pub fn new(name: &str) -> Self {
            return Task {
                start: None,
                end: None,
                description: name.to_string(),
                annotations: Vec::new(),
            };
        }

        pub fn start(&mut self) {
            self.start = Some(now_serialised());
        }

        pub fn annotate(&mut self, text: &str) {
            self.annotations.push(Annotation {
                created: now_serialised(),
                text: text.to_string(),
            });
        }

        pub fn done(&mut self) {
            let now = now_serialised();
            if self.start.is_none() {
                self.start = Some(now.clone());
            }
            self.end = Some(now);
        }

        pub fn status(&self) -> Status {
            if self.start.is_none() {
                Status::New
            } else if self.end.is_some() {
                Status::Done
            } else {
                Status::InProgress
            }
        }
    }

    #[derive(Default, Debug, Clone, Reconcile, Hydrate, PartialEq)]
    pub struct Tasks {
        pub tasks: Vec<Task>,
    }
}

struct Fs {
    fs_storage: automerge_repo::fs_store::FsStore,
}

impl Fs {
    pub fn new(p: std::path::PathBuf) -> Self {
        let fs_storage =
            automerge_repo::fs_store::FsStore::open(p).expect("could not open the storage");
        Fs { fs_storage }
    }
}

impl automerge_repo::Storage for Fs {
    fn get(&self, id: DocumentId) -> BoxFuture<'static, Result<Option<Vec<u8>>, StorageError>> {
        Box::pin(futures::future::ready(
            self.fs_storage
                .get(&id)
                .map_err(move |_| StorageError::Error),
        ))
    }

    fn list_all(&self) -> BoxFuture<'static, Result<Vec<DocumentId>, StorageError>> {
        Box::pin(futures::future::ready(
            self.fs_storage.list().map_err(move |_| StorageError::Error),
        ))
    }

    fn append(
        &self,
        id: DocumentId,
        changes: Vec<u8>,
    ) -> BoxFuture<'static, Result<(), StorageError>> {
        Box::pin(futures::future::ready(
            self.fs_storage
                .append(&id, &changes)
                .map_err(move |_| StorageError::Error),
        ))
    }

    fn compact(&self, _: DocumentId, _: Vec<u8>) -> BoxFuture<'static, Result<(), StorageError>> {
        Box::pin(futures::future::ready(Ok(())))
    }
}

fn list(
    doc_handle: automerge_repo::DocHandle,
    with_annotations: bool,
    show: HashMap<task::Status, bool>,
) {
    doc_handle.with_doc(|doc| {
        let tasks: Tasks = hydrate(doc).unwrap();
        for (i, task) in tasks.tasks.iter().enumerate() {
            if show[&task.status()] {
                println!("{} {} {}", i + 1, task.status().symbol(), task.description);
                if with_annotations {
                    for annotation in &task.annotations {
                        println!(
                            "\t{} {}",
                            annotation
                                .created()
                                .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
                            annotation
                        )
                    }
                }
            }
        }
    });
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    let repo_handle = {
        let dir = match env::var("TSK_DATA_DIR") {
            Ok(custom_dir) => PathBuf::from(custom_dir),
            _ => {
                let xdg_dirs = xdg::BaseDirectories::with_prefix("tsk").unwrap();
                xdg_dirs.get_data_home()
            }
        };
        let data_dir = Fs::new(dir);
        let repo = automerge_repo::Repo::new(None, Box::new(data_dir));
        repo.run()
    };
    let doc_handle = {
        let docs = repo_handle
            .list_all()
            .await
            .expect("failed to list documents");
        if docs.is_empty() {
            let d = repo_handle.new_document();
            let tasks = Tasks { tasks: Vec::new() };
            d.with_doc_mut(|doc| {
                let mut tx = doc.transaction();
                reconcile(&mut tx, &tasks).unwrap();
                tx.commit();
            });
            d
        } else {
            let doc_id = &docs[0];
            repo_handle
                .load(doc_id.clone())
                .await
                .expect("failed to open the document")
                .unwrap()
        }
    };

    let matches = command!()
        .subcommand(
            Command::new("add").about("Add one task").arg(
                arg!(<description> "description of the task")
                    .action(ArgAction::Set)
                    .required(true),
            ),
        )
        .subcommand(
            Command::new("start").about("Start one task").arg(
                arg!(<number> "number of the task")
                    .action(ArgAction::Set)
                    .required(true),
            ),
        )
        .subcommand(
            Command::new("annotate")
                .about("Annotate one task")
                .arg(
                    arg!(<number> "number of the task")
                        .action(ArgAction::Set)
                        .required(true),
                )
                .arg(
                    arg!(<text> "text of the annotation")
                        .action(ArgAction::Set)
                        .required(true),
                ),
        )
        .subcommand(
            Command::new("done").about("Complete one task").arg(
                arg!(<number> "number of the task")
                    .action(ArgAction::Set)
                    .required(true),
            ),
        )
        .subcommand(
            Command::new("list")
                .about("List the tasks")
                .arg(arg!(-a --annotations "show annotations").action(ArgAction::SetTrue))
                .arg(arg!(--status <STATUS> "filter by status").action(ArgAction::Append)),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("add", sub)) => {
            let description = sub
                .get_one::<String>("description")
                .expect("task description is requried");

            let t = Task::new(description);
            doc_handle.with_doc_mut(|doc| {
                let mut tasks: Tasks = hydrate(doc).unwrap();
                tasks.tasks.push(t);
                let mut tx = doc.transaction();
                reconcile(&mut tx, &tasks).unwrap();
                tx.commit();
            });
        }

        Some(("annotate", sub)) => {
            let number = sub
                .get_one::<String>("number")
                .expect("task number is requried");
            let number = number
                .parse::<usize>()
                .expect("task number is expected to be a number");
            if number == 0 {
                return Err(anyhow!("no task number {}", number));
            }
            let index = number - 1;
            let text = sub
                .get_one::<String>("text")
                .expect("note text is requried");

            doc_handle.with_doc_mut(|doc| {
                let mut tasks: Tasks = hydrate(doc).unwrap();
                let t = tasks
                    .tasks
                    .get_mut(index)
                    .expect(&format!("no task number {}", number));

                t.annotate(text);

                let mut tx = doc.transaction();
                reconcile(&mut tx, &tasks).unwrap();
                tx.commit();
            });
        }

        Some(("start", sub)) => {
            let number = sub
                .get_one::<String>("number")
                .expect("task number is requried");
            let number = number
                .parse::<usize>()
                .expect("task number is expected to be a number");
            if number == 0 {
                return Err(anyhow!("no task number {}", number));
            }
            let index = number - 1;

            doc_handle.with_doc_mut(|doc| {
                let mut tasks: Tasks = hydrate(doc).unwrap();
                let t = tasks
                    .tasks
                    .get_mut(index)
                    .expect(&format!("no task number {}", number));

                t.start();

                let mut tx = doc.transaction();
                reconcile(&mut tx, &tasks).unwrap();
                tx.commit();
            });
        }

        Some(("done", sub)) => {
            let number = sub
                .get_one::<String>("number")
                .expect("task number is requried");
            let number = number
                .parse::<usize>()
                .expect("task number is expected to be a number");
            if number == 0 {
                return Err(anyhow!("no task number {}", number));
            }
            let index = number - 1;

            doc_handle.with_doc_mut(|doc| {
                let mut tasks: Tasks = hydrate(doc).unwrap();
                let t = tasks
                    .tasks
                    .get_mut(index)
                    .expect(&format!("no task number {}", number));

                t.done();

                let mut tx = doc.transaction();
                reconcile(&mut tx, &tasks).unwrap();
                tx.commit();
            });
        }

        Some(("list", sub)) => {
            let with_annotations = sub.get_flag("annotations");
            let show: HashMap<task::Status, bool> = {
                match sub.get_many::<String>("status") {
                    Some(statuses) => {
                        let mut show = HashMap::from([
                            (task::Status::New, false),
                            (task::Status::InProgress, false),
                            (task::Status::Done, false),
                        ]);

                        for s in statuses {
                            match s.as_ref() {
                                "new" => show
                                    .entry(task::Status::New)
                                    .and_modify(|show| *show = true),
                                "in-progress" => show
                                    .entry(task::Status::InProgress)
                                    .and_modify(|show| *show = true),
                                "done" => show
                                    .entry(task::Status::Done)
                                    .and_modify(|show| *show = true),
                                invalid => panic!(
                                    "invalid status {}. Valid statuses: {}, {}, {}",
                                    invalid, "new", "in-progress", "done"
                                ),
                            };
                        }
                        show
                    }
                    None => HashMap::from([
                        (task::Status::New, true),
                        (task::Status::InProgress, true),
                        (task::Status::Done, true),
                    ]),
                }
            };

            list(doc_handle, with_annotations, show)
        }
        _ => {
            // When called without arguments, list non-completed tasks
            list(
                doc_handle,
                false,
                HashMap::from([
                    (task::Status::New, true),
                    (task::Status::InProgress, true),
                    (task::Status::Done, false),
                ]),
            );
        }
    }

    repo_handle
        .stop()
        .expect("failed to cleanly close the repository");

    Ok(())
}
